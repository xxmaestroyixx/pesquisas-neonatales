<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /** Usuarios por defecto */
        $this->call(UsersSeeder::class);
        /** Estados */
        $this->call(StateSeeder::class);
        /** Ciudades */
        $this->call(CitySeeder::class);
        /** Generos */
        $this->call(GenderSeeder::class);
    }
}
