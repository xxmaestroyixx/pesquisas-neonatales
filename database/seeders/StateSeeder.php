<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\State;
use DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            "01" => "Distrito Capital",
            "02" => "Amazonas",
            "03" => "Anzoategui",
            "04" => "Apure",
            "05" => "Aragua",
            "06" => "Barinas",
            "07" => "Bolívar",
            "08" => "Carabobo",
            "09" => "Cojedes",
            "10" => "Delta Amacuro",
            "11" => "Falcón",
            "12" => "Guárico",
            "13" => "Lara",
            "14" => "Mérida",
            "15" => "Miranda",
            "16" => "Monagas",
            "17" => "Nueva Esparta",
            "18" => "Portuguesa",
            "19" => "Sucre",
            "20" => "Táchira",
            "21" => "Trujillo",
            "22" => "Yaracuy",
            "23" => "Zulia",
            "24" => "La Guaira"
        ];

        DB::transaction(function () use ($states) {
            foreach ($states as $key => $state) {
                State::updateOrCreate(
                    ['code' => $key],
                    ['name' => $state]
                );
            }
        });
    }
}
