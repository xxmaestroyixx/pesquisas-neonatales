<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_records', function (Blueprint $table) {
            $table->id();
            $table->string('number')->comment('Número de historia clínica');
            $table->date('sample_taken_at')->comment('Fecha de la toma de la muestra');
            $table->boolean('first_sample')->default(false)->comment('Primera muestra');
            $table->boolean('second_sample')->default(false)->comment('Segunda muestra');
            $table->float('birth_weight')->default(0)->comment('Peso al nacer');
            $table->integer('pregnancy_duration')->default(0)->comment('Duración del embarazo');
            $table->boolean('take_antibiotics')->default(false)->comment('Toma antibióticos?');
            $table->boolean('background')->default(false)->comment('Trasfundo o antecedentes');
            $table->enum('birth_type', ['N', 'F', 'C'])
                  ->comment('Tipo de parto: (N)atural, (F)orceps, (C)esarea');
            $table->boolean('consanguineous_parents')->default(false)->comment('Padres consanguíneos');
            $table->enum('lactation_type', ['M', 'A'])->default('M')
                  ->comment('Tipo de lactancia: (M)atern, (A)rtifitial');
            $table->foreignId('patient_id')->constrained()->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_records');
    }
}
