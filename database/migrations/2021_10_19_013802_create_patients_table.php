<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('register_number')->comment('Número de registro');
            $table->string('first_surname')->comment('Primer Appellido');
            $table->string('second_surname')->comment('Segundo Apellido')->nullable();
            $table->string('first_name')->comment('Primer nombre');
            $table->string('middle_name')->comment('Inicial del segundo nombre')->nullable();
            $table->enum('sex', ['M', 'F'])->comment('Sexo');
            $table->date('birthdate')->comment('Fecha de nacimiento');
            $table->foreignId('mother_id')->constrained()->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
