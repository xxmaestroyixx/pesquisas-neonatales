<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMothersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mothers', function (Blueprint $table) {
            $table->id();
            $table->string('identity_card', 10)->comment('Cédula de Identidad');
            $table->string('first_surname')->comment('Primer Appellido');
            $table->string('second_surname')->comment('Segundo Apellido')->nullable();
            $table->string('first_name')->comment('Primer nombre');
            $table->string('middle_name')->comment('Inicial del segundo nombre')->nullable();
            $table->date('birthdate')->comment('Fecha de nacimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mothers');
    }
}
