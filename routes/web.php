<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\StateController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\DiseaseController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\ResultController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\OrganismController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth'], 'prefix' => 'api/v1'], function() {
    /** Gestión de géneros */
    Route::apiResource('genders', GenderController::class);
    /** Gestión de Estados */
    Route::apiResource('states', StateController::class);
    Route::get('list/states', [StateController::class, 'list']);
    /** Gestión de Organismos */
    Route::apiResource('organisms', OrganismController::class);
    Route::get('list/organisms/{stateId?}', [OrganismController::class, 'list']);
    /** Gestión de Ciudades */
    Route::apiResource('cities', CityController::class);
    /** Gestión de enfermedades */
    Route::apiResource('diseases', DiseaseController::class);
    /** Gestión de pacientes */
    Route::apiResource('patients', PatientController::class);
    Route::post('search-mother', [PatientController::class, 'searchMother']);
    Route::post('search-patient', [PatientController::class, 'search']);
    /** Gestión de resultados */
    Route::apiResource('results', ResultController::class);
    Route::post('export-results', [ResultController::class, 'export']);
    Route::get('export-results/{file_name}', function ($file_name) {
        $path = storage_path('app/' .$file_name);
        $file = File::get($path);
        /** @var array|string Tipo de archivo */
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });
    /** Gestión de usuarios */
    Route::apiResource('users', UserController::class);
    Route::post('profile/users', [UserController::class, 'updateProfile']);
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/{any}', function () {
        return view('layouts.app');
    })->where('any', '.*');
});