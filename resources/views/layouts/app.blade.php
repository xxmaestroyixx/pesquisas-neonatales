<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon.png') }}">
        <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="Gestión de pesquisas neonatales, Venezuela">
	    <meta name="author" content="CENDITEL Noto Mérida, www.cenditel.gob.ve">
	    <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title>{{ config('app.name') }} | {{ __('Acceso') }}</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	    <link
	    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
	    rel="stylesheet">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/4.1.4/css/sb-admin-2.min.css" integrity="sha512-Mk4n0eeNdGiUHlWvZRybiowkcu+Fo2t4XwsJyyDghASMeFGH6yUXcdDI3CKq12an5J8fq4EFzRVRdbjerO3RmQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	    {{-- Toastr --}}
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" defer></script>
	    @yield('extra-css')
        <script>
            window.url = "{{ config('app.url') }}";
            window.debug = {{ config('app.debug') }};
        </script>
	    <style>
	    	.sidebar-brand-icon img {
	    		max-height: 50px;
	    	}
	    	.close {
	    		font-size: 1rem;
	    	}
	    	.VueTables .pull-left {
	    		float: left !important;
	    	}
	    	.VueTables .pull-right {
	    		float: right !important;
	    	}
	    	.VueTables__heading {
	    		padding-right: 5px !important;
	    	}
	    	.VuePagination nav {
	    		margin-left: auto !important;
	    		margin-right: auto !important;
	    	}
	    </style>
	</head>
	<body id="page-top">

		<div id="app">
			<app :user="{{ auth()->user() }}" logout-url="{{ route('logout') }}" domain="{{ config('app.url') }}"></app>
		</div>

		<a class="scroll-to-top rounded" href="#page-top">
	        <i class="fas fa-angle-up"></i>
	    </a>

		<script src="{{ asset('js/manifest.js') }}"></script>
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/startbootstrap-sb-admin-2/4.1.4/js/sb-admin-2.min.js" integrity="sha512-+QnjQxxaOpoJ+AAeNgvVatHiUWEDbvHja9l46BHhmzvP0blLTXC4LsvwDVeNhGgqqGQYBQLFhdKFyjzPX6HGmw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        @yield('extra-js')
	</body>
</html>