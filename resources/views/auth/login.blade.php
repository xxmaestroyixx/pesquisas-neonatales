@extends('layouts.auth')

@section('content')
    <div class="p-5">
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">{{ __('Acceso') }}</h1>
        </div>
        <form class="user" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input type="text" name="username" class="form-control form-control-sm @error('username') is-invalid @enderror" 
                       placeholder="{{ __('Usuario') }}" required autocomplete="username" autofocus>
                @error('username')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control form-control-sm @error('password') is-invalid @enderror" 
                       placeholder="{{ __('Contraseña') }}" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="remember" name="remember" 
                           {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="remember">{{ __('Recuerdame') }}</label>
                </div>
            </div>
            <button class="btn btn-primary btn-block">Acceder</button>
        </form>
        @if (Route::has('password.request'))
            <hr>
            <div class="text-center">
                <a class="small" href="{{ route('password.request') }}">
                    {{ __('¿Olvidó la contraseña?') }}
                </a>
            </div>
        @endif
    </div>
@endsection
