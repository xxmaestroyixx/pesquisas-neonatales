@extends('layouts.auth')

@section('content')
    <div class="p-5">
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">{{ __('Restablecer contraseña') }}</h1>
        </div>
        <form method="POST" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <input type="email" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" 
                       value="{{ $email ?? old('email') }}" placeholder="{{ __('Correo electrónico') }}" required 
                       autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" 
                       name="password" placeholder="{{ __('Contraseña') }}" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <input type="password" class="form-control form-control-sm" name="password_confirmation" 
                       placeholder="{{ __('Confirmar contraseña') }}" required autocomplete="new-password">
            </div>

            <button class="btn btn-primary btn-block">
                {{ __('Restablecer contraseña') }}
            </button>
        </form>
    </div>
@endsection
