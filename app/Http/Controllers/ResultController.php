<?php

namespace App\Http\Controllers;

use App\Models\Result;
use App\Models\MedicalRecord;
use Illuminate\Http\Request;
use App\Exports\ReportResults;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results = Result::with(['medicalRecord' => function ($q) {
            $q->with('patient', function ($qq) {
                $qq->with('mother');
            });
        }])->get();

        return $this->success($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'medical_record.id' => ['required'],
            'diagnosed_at' => ['required', 'date']
        ], [
            'medical_record.id.required' => 'El registro de historia clinica es obligatorio.',
            'diagnosed_at.required' => 'El campo fecha de diagnóstico es obligatorio.',
            'diagnosed_at.date' => 'El campo fecha de diagnóstico tiene un formato inválido.'
        ]);

        $result = DB::transaction(function () use ($request) {
            $medicalRecord = MedicalRecord::find($request->medical_record['id']);
            if ($medicalRecord) {
                $result = $medicalRecord->result()->updateOrCreate(
                    ['medical_record_id' => $request->medical_record['id']], $request->input()
                );
            }
            return $result;
        });

        return $this->success($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
        $result = Result::with(['medicalRecord' => function ($q) {
            $q->with('patient', function ($qq) {
                $qq->with('mother');
            });
        }])->where('id', $result->id)->first();
        return $this->success($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        $this->validate($request, [
            'medical_record.id' => ['required'],
            'diagnosed_at' => ['required', 'date']
        ], [
            'medical_record.id.required' => 'El registro de historia clinica es obligatorio.',
            'diagnosed_at.required' => 'El campo fecha de diagnóstico es obligatorio.',
            'diagnosed_at.date' => 'El campo fecha de diagnóstico tiene un formato inválido.'
        ]);

        $result = DB::transaction(function () use ($request, $result) {
            $result->update($request->input());
            return $result;
        });

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        DB::transaction(function () use ($result) {
            $result->delete();
        });

        return $this->success([]);
    }

    /**
     * Exportar resultados
     *
     * @method export
     *
     * @author Ing. Roldan Vargas <rvargas@cenditel.gob.ve> | <roldandvg@gmail.com>
     *
     * @param  Request $request Opciones de la petición
     *
     * @return JsonResponse     Datos de respuesta
     */
    public function export(Request $request)
    {
        $fromDate = ($request->fromDate!==null)?$request->fromDate:null;
        $toDate = ($request->toDate!==null)?$request->toDate:null;
        $results = Excel::store(new ReportResults($fromDate, $toDate), 'results.xlsx');
        return $this->success(['fileName' => 'results.xlsx']);
    }
}
