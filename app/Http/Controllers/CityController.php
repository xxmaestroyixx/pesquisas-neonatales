<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use DB;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::with('state')->get();

        return $this->success($cities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'state_id' => ['required'],
            'name' => ['required']
        ]);

        $city = DB::transaction(function () use ($request) {
            $city = City::create($request->input());
            return $city;
        });

        return $this->success($city);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        return $this->success($city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $this->validate($request, [
            'state_id' => ['required'],
            'name' => ['required']
        ]);

        $city = DB::transaction(function () use ($request, $city) {
            $city->update($request->input());
            return $city;
        });

        return $this->success($city);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        DB::transaction(function () use ($city) {
            $city->delete();
        });
        return $this->success([]);
    }
}
