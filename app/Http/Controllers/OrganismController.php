<?php

namespace App\Http\Controllers;

use App\Models\Organism;
use Illuminate\Http\Request;
use DB;

class OrganismController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organisms = Organism::with('state')->get();

        return $this->success($organisms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
            'state_id' => ['required']
        ]);

        $organism = DB::transaction(function () use ($request) {
            $organism = Organism::create($request->input());
            return $organism;
        });

        return $this->success($organism);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function show(Organism $organism)
    {
        return $this->success($organism);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organism $organism)
    {
        $this->validate($request, [
            'name' => ['required'],
            'state_id' => ['required']
        ]);

        $organism = DB::transaction(function () use ($request, $organism) {
            $organism->update($request->input());
            return $organism;
        });

        return $this->success($organism);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organism  $organism
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organism $organism)
    {
        
        DB::transaction(function () use ($organism) {
            $organism->delete();
        });
        return $this->success([]);
    }

    /**
     * Listado de organismos
     * 
     * @param  string $stateId Identificador del Estado al que pertenece un organismo. Opcional
     * 
     * @return JsonResponse    Objeto JSON con listado de registros
     */
    public function list($stateId = null)
    {
        $organisms = Organism::select('id', 'name');
        if ($stateId!==null) {
            $organisms = $organisms->where('state_id', $stateId);
        }

        return $this->success($organisms->get());
    }
}
