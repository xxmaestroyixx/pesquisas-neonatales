<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCreated extends Notification
{
    use Queueable;

    public $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from(env('MAIL_FROM_ADDRESS', 'admin@idea.gob.ve'), env('MAIL_FROM_NAME', 'Pesquisas Neonatales'))
                    ->subject('Nuevo usuario - Pesquisas Neonatales')
                    ->line(
                        'Te notificamos que se ha creado un usuario en la plataforma de Pesquisas Neonatales con las siguientes ' .
                        'credenciales de acceso'
                    )
                    ->line('Usuario: ' . $notifiable->username)
                    ->line('Contraseña: ' . $this->password)
                    ->line('En el primer acceso es recomendable modificar la contraseña desde la opción de perfil de usuario');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
