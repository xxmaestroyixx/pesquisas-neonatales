<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['code', 'name'];

    /**
     * State has many Cities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }

    /**
     * State has many Organisms.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisms()
    {
        return $this->hasMany(Organism::class);
    }
}
