<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mother extends Model
{
    use HasFactory;

    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'identity_card', 'first_surname', 'second_surname', 'first_name', 'middle_name', 'birthdate'
    ];

    protected $appends = ['full_name'];

    protected $casts = [
        'birthdate' => 'date:Y-m-d'
    ];

    public function getFullNameAttribute()
    {
        $fullName = $this->first_surname . ' ' . $this->second_surname . ' ' . $this->first_name . ' ' . 
                    $this->middle_name;
        return trim($fullName);
    }

    /**
     * Mother has many Patients.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function patients()
    {
        return $this->hasMany(Patient::class);
    }
}
